<!doctype html>
<html>
<head>
  <meta charset="utf-8">
  <title>HealthPlus</title>
  <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
  <script src="//code.jquery.com/jquery-1.10.2.js"></script>
  <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
	
	<!-- Latest compiled and minified CSS -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">

	<!-- Optional theme -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap-theme.min.css" integrity="sha384-fLW2N01lMqjakBkx3l/M9EahuwpSfeNvV63J5ezn3uZzapT0u7EYsXMjQV+0En5r" crossorigin="anonymous">

	<!-- Latest compiled and minified JavaScript -->
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
	</head>



	<body>

	<div class="col-md-4">
		<div class="login-panel panel panel-default">
				<div class="panel-heading">
						<h3 class="panel-title">Sign In</h3>
				</div>
				<div class="panel-body">
						<form role="form" action="http://localhost/HealthPlus/index.php" method="post">
						
						<?php 
						
						if(isset($_SESSION['error'])){
							if($_SESSION['error'] == '400'){
								require('./error_400.tpl');
							}
						}
						
						?>
						
						
								<fieldset>
										<div class="form-group">
												<input class="form-control" placeholder="Login" name="login" type="email" autofocus="">
										</div>
										<div class="form-group">
												<input class="form-control" placeholder="Password" name="password" type="password" value="">
										</div>
										<!-- Change this to a button or input when using this as a form -->
										<p><input type="submit" class="btn btn-sm btn-success" value="Login"></p>
								</fieldset>
						</form>
				</div>
		</div>
	</div>

	</body>

</html>