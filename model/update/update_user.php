<?php
function update_user($id, $bmi)
{
    global $db;
        
    $req = $db->prepare('UPDATE user SET bmi = :bmi WHERE id = :id');
    $req->bindParam(':id', $id, PDO::PARAM_INT);
    $req->bindParam(':bmi', $bmi, PDO::PARAM_STR);
		
    if($req->execute()){
			return true;
		} else {
			return false;
		}
}
