<?php
function select_user($login, $password)
{
    global $db;
        
    $req = $db->prepare('SELECT id FROM user WHERE login = :login AND password = :password');
    $req->bindParam(':login', $login, PDO::PARAM_STR);
    $req->bindParam(':password', $password, PDO::PARAM_STR);
		
    $req->execute();
    $res = $req->fetchAll();
    
    
    return $res;
}
