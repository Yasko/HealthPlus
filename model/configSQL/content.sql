use SocialNetwork;

ALTER TABLE Membre AUTO_INCREMENT=0;
ALTER TABLE Theme AUTO_INCREMENT=0;
ALTER TABLE Commentaire AUTO_INCREMENT=0;

REPLACE INTO Membre(prenom, nom, pseudo, password, email)
	VALUES	('Natsy', 'Bouiti-Viaudo', 'natsybv', '777', 'natsybv@gmail.com'),
			('Yassin', 'Hassan', 'yasko', '777', 'yasko@gmail.com'),
			('Kevin', 'Durant', 'KD8', '777', 'kd8@gmail.com'),
			('Cristiano', 'Ronaldo', 'CR7', '777', 'cr7@gmail.com');

REPLACE INTO Theme(nom_theme)
	VALUES  ('Sport'),
			('Politique'),
			('Economie'),
			('Social'),
			('Musique'),
			('Cinéma'),
			('Faits divers'),
			('Informatique'),
			('Art');

REPLACE INTO Commentaire(id_auteur, id_theme, contenu, niveau, nb_like, nb_unlike)
	VALUES	('1', '1', "Qui est chaud pour un urban demain ?", 'U', '6', '1'),
			('1', '2', "La politique a perdu son sens d'origine, c'est devenu de la comédie", 'G', '2', '1'),
			('1', '1', "j'aime le basketball c'est un sport plein d'intensité", 'O', '10', '0'),
			('2', '8', "Ce p... de projet de PWEB est grave chaud", 'U', '4', '0'),
			('2', '3', "FR - BFR = TN", 'G', '2', '0'),
			('2', '6', "Le Labyrinthe : La Terre brûlée un des meilleurs film de cette année", 'O', '5', '0'),
			('3', '7', "Le rer C est encore en retard !!! la ratp me saoule", 'U', '2', '0'),
			('3', '4', "Attila de B2O, une tuerie", 'G', '3', '2'),
			('3', '4', "l'art ne sert à rien !", 'O', '2', '4');
			
	
	
	