use SocialNetwork;

Drop table if exists Unliker;
Drop table if exists Liker;
Drop table if exists Commentaire;
Drop table if exists Theme;
Drop table if exists Contact;
Drop table if exists Membre;
Drop table if exists Message;

CREATE TABLE Membre (
id_membre INTEGER NOT NULL PRIMARY KEY AUTO_INCREMENT,
prenom VARCHAR(20) NOT NULL,
nom VARCHAR(20) NOT NULL,
pseudo VARCHAR(20) UNIQUE NOT NULL,
password VARCHAR(20) NOT NULL,
email VARCHAR(20) NOT NULL,
photo varchar(128),
date_inscription DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
nb_commentaires INTEGER NOT NULL DEFAULT 0,
CONSTRAINT uc_Membre UNIQUE (pseudo)
)
engine=innodb character set utf8 collate utf8_unicode_ci;


CREATE TABLE Contact (
id_membre INTEGER NOT NULL,
id_contact INTEGER NOT NULL,
CONSTRAINT pk_Contact PRIMARY KEY (id_membre,id_contact),
constraint fk_Membre foreign key(id_membre) references Membre(id_membre),
constraint fk_Contact foreign key(id_contact) references Membre(id_membre)
)
engine=innodb character set utf8 collate utf8_unicode_ci;


CREATE TABLE Theme (
id_theme INTEGER NOT NULL PRIMARY KEY AUTO_INCREMENT,
nom_theme VARCHAR(20) NOT NULL,
frequence INTEGER NOT NULL DEFAULT 0,
CONSTRAINT uc_Theme UNIQUE (nom_theme)
)
engine=innodb character set utf8 collate utf8_unicode_ci;


CREATE TABLE Commentaire (
id_commentaire INTEGER NOT NULL PRIMARY KEY AUTO_INCREMENT,
id_auteur INTEGER NOT NULL,
id_theme INTEGER NOT NULL,
contenu VARCHAR(2000) NOT NULL,
nb_like INTEGER NOT NULL DEFAULT 0,
nb_unlike INTEGER NOT NULL DEFAULT 0,
niveau CHAR(1) NOT NULL DEFAULT 'U', -- U, G, O user (privé), group (contact), other (public)
date_publication DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
constraint fk_com_Theme foreign key(id_theme) references Theme(id_theme),
constraint fk_com_Membre foreign key(id_auteur) references Membre(id_membre)
)
engine=innodb character set utf8 collate utf8_unicode_ci;


CREATE TABLE Liker (
	id_membre INTEGER NOT NULL,
	id_commentaire INTEGER NOT NULL,
	CONSTRAINT pk_Liker PRIMARY KEY (id_membre,id_commentaire),
	constraint fk_liker_Membre foreign key(id_membre) references Membre(id_membre),
	constraint fk_liker_Commentaire foreign key(id_commentaire) references Commentaire(id_commentaire)
)
engine=innodb character set utf8 collate utf8_unicode_ci;

CREATE TABLE Unliker (
	id_membre INTEGER NOT NULL,
	id_commentaire INTEGER NOT NULL,
	CONSTRAINT pk_Unliker PRIMARY KEY (id_membre,id_commentaire),
	constraint fk_unliker_Membre foreign key(id_membre) references Membre(id_membre),
	constraint fk_unliker_Commentaire foreign key(id_commentaire) references Commentaire(id_commentaire)
)
engine=innodb character set utf8 collate utf8_unicode_ci;

CREATE TABLE Message (
	id_message INTEGER NOT NULL PRIMARY KEY AUTO_INCREMENT,
	contenu VARCHAR(2000) NOT NULL,
	id_expediteur INTEGER NOT NULL,
	id_recepteur INTEGER NOT NULL,
	constraint fk_mes_Membre foreign key(id_expediteur) references Membre(id_membre),
	constraint fk_mes_Contact foreign key(id_recepteur) references Membre(id_membre)
)
engine=innodb character set utf8 collate utf8_unicode_ci;
