CREATE DATABASE if not exists SocialNetwork character set utf8 collate utf8_unicode_ci;
use SocialNetwork;

grant all privileges on SocialNetwork.* to 'admin_user'@'localhost' identified by 'secret';
