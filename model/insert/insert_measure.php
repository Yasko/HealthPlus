<?php
function insert_user($user, $weight, $height, $date)
{
    global $db;
        
    $req = $db->prepare('INSERT INTO measure (user, weight, height, date)
 VALUES (:user, :weight, :height, :date)');
    $req->bindParam(':user', $user, PDO::PARAM_INT);
		$req->bindParam(':weight', $weight, PDO::PARAM_STR);
		$req->bindParam(':height', $height, PDO::PARAM_STR);
		$req->bindParam(':date', $date, PDO::PARAM_STR);
		
    if($req->execute()){
			return true;
		} else {
			return false;
		}
}
