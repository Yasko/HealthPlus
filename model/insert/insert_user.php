<?php
function insert_user($login, $password, $firstname, $lastname, $birthdate)//pas de bmi à la création
{
    global $db;
        
    $req = $db->prepare('INSERT INTO user (login, password, firstname, lastname, birthdate)
 VALUES (:login, :password, :firstname, :lastname, :birthdate)');
    $req->bindParam(':login', $login, PDO::PARAM_STR);
		$req->bindParam(':password', $password, PDO::PARAM_STR);
		$req->bindParam(':firstname', $firstname, PDO::PARAM_STR);
		$req->bindParam(':lastname', $lastname, PDO::PARAM_STR);
		$req->bindParam(':birthdate', $birthdate, PDO::PARAM_STR);
		
    if($req->execute()){
			return true;
		} else {
			return false;
		}
}
