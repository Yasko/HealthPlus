<?php
function db_connect() {
    // définition des variables de connexion à la base de données   
    try {
        $pdo_options[PDO::ATTR_ERRMODE] = PDO::ERRMODE_EXCEPTION;
        // INFORMATIONS DE CONNEXION
        $host =     'localhost';
        $dbname =   'database';
				$charset =   'utf8';
        $user =     'root';
        $password =     ''; //pas de mot de passe sous WAMP
        // FIN DES DONNEES
        
        $db = new PDO('mysql:host='.$host.';dbname='.$dbname.';charset'.$charset.'', $user, $password, $pdo_options);
        return $db;
    } catch (Exception $e) {
        die('Erreur de connexion : ' . $e->getMessage());
    }
}
