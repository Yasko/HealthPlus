<?php
require_once('controller/request_manager/get_user.php');
/*
 * Vérifie si un utilisateur correspondant au couple login/password existe
 * @param login 
 * @param password
 * @return true si il existe, false sinon
 */
function exist_user($login, $password){
	$res = get_user($login, $password);
	if($res != null){
		return true;
	} else {
		return false;
	}
}