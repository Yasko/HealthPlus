<?php
require_once('/model/select/select_user.php');

function get_user($login, $password){
	$res = select_user($login, $password);
	if(count($res)>0){
		return $res[0]['id'];
	} else {
		return null;
	}
}
