<?php
if(exist_user($_POST['login'], $_POST['password'])){
	//l'authentification est réussie
	$_SESSION['error'] = null; // donc pas d'erreur
	$_SESSION['id'] = get_user($_POST['login'], $_POST['password']); //nouvelle variable de Session
	$_SESSION['login'] = $_POST['login'];
}
else{
	//le couple login/password n'existe pas 
	$_SESSION['error'] = '400'; //erreur qui s'affichera dans user_connect.tpl
	require('/view/user_connect.tpl');
}